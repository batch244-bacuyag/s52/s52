import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';




function App() {
  return (
//In react JS, multiple components rendered in a single component should be wrapped in a parent components
//"Fragment" ensures that an error will be prevented.
//you can use <></> replacing fragment
    <Fragment>
        <AppNavbar/>
{/*        <Home/>*/}
    <Container>
{/*        <Courses/>*/}
{/*        <Register/>*/}
        <Login/>
    </Container>

    </Fragment>

  );
}

export default App;
   