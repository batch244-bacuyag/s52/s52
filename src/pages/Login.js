import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password);

  function Login(e) {
    e.preventDefault();

    setEmail('');
    setPassword('');

    alert('You have successfully logged in!');
  }

  useEffect(() => {
    if (email && password) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Form onSubmit={(e) => Login(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <>
          <br />
          <Button variant="primary" type="submit" id="submitBtn">
            Submit
          </Button>
        </>
      ) : (
        <>
          <br />
          <Button variant="danger" type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        </>
      )}
    </Form>
  );
}
