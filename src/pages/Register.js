import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Register(){

	//state hooks to store the values of the inout fields
	const [ email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	//check if the values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e){

		e.preventDefault();

		//clear input fileds 
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!')
	}

	useEffect(() => {
		if((email && password1 && password2) && (password1 === password2)) {
			setIsActive(true);
		} else{
			setIsActive(false);
		}

	}, [email, password1, password2]);

	return (
		//Invokes the registerUser function upon clicking the submit btn
		<Form onSubmit = {(e) => registerUser(e)}>
			<Form.Group controlId = "userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter email"
					required
					value = {email}
					onChange = {e => setEmail(e.target.value)}
				/>
				<Form.Text className = "text-muted">We'll never share your email with anyone else.</Form.Text>
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Password"
					required
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId = "password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type = "password"
					placeholder = "Verify Password"
					required
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
				/>
			</Form.Group>

			{/*conditional render submit button based on isActive state}*/}

			{isActive
				?
					<>
					<br />
					<Button variant = "primary" type = "submit" id = "submitBtn">Submit</Button>
					</>
				:
					<>
					<br />
					<Button variant = "danger" type = "submit" id = "submitBtn" disabled>Submit</Button>
					</>
			}
			

		</Form>
	);
}
